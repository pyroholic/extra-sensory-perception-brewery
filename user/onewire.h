/*
 * onewire.h
 *
 *  Created on: Apr 22, 2017
 *      Author: julle
 */

#ifndef USER_ONEWIRE_H_
#define USER_ONEWIRE_H_

int readDS18B20(int ow, float *temperature);

int ds18b20_startConversion(int ow);

int onewire_init(unsigned short pin);
#endif /* USER_ONEWIRE_H_ */
