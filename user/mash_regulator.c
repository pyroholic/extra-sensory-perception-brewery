/*
 * mash_regulator.c
 *
 *  Created on: Apr 16, 2017
 *      Author: julle
 */
#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
#include "freertos/task.h"
#include "pwm.h"
#include "gpio.h"
#include "mash_regulator.h"
#include "espressif/esp8266/pin_mux_register.h"
#include "onewire.h"
#include <string.h>
#include "espressif/esp8266/eagle_soc.h"
#include "c_types.h"

#define KPRIMS 15
#define KPMASH 0
#define KIRIMS 0.0001f

xTimerHandle mash_timer;
unsigned int clock = 0;
mash_entry_type nullmash = {0,0};
float mash_setTemp = 0;
int onewire_handle = -1;
int onewire_handle_rims = -1;
float mashTemp = 0.0;
float rimsTemp = 0.0;

mash_states mash_state = MASH_STATE_NOT_STARTED;

mash_entry_type mash_schedule[MAX_MASH_ENTRIES];

void setMashSetTemp(float temp)
{
	mash_setTemp = temp;
}

float getMashSetTemp(void)
{
	return mash_setTemp;
}

float getMashActualTemp(void)
{
	return mashTemp;
}

float getRimsActualTemp(void)
{
	return rimsTemp;
}

uint32 calculateDuty(float inMashSetTemp, float inMashTemp, float inRimsTemp)
{
	float eRims;
	float eMash;
	static float eSum = 0;
	int32 retVal = 0;

	if(inRimsTemp > 90.0)
	{
		retVal = 0;
	}
	else
	{
		eRims = inMashSetTemp - inRimsTemp;
		eMash = inMashSetTemp - inMashTemp;
		eSum = eSum + eRims;
		retVal = (int32)((eSum * KIRIMS) + (eRims * KPRIMS) + (eMash * KPMASH));


		if (retVal > 100)
		{
			retVal = 100;
		}
		else if (retVal < 0)
		{
			retVal = 0;
		}
	}
	return (uint32)retVal;
}

void vMashTimerCallback( xTimerHandle xTimer )
{
	unsigned int timeCollector;
	int idx = 0;
	int startConversionResult, readMashTempFailed, readRimsTempFailed;
	static uint8 nrFailedReadTemps = 0;
	uint32 duty_dig = 0;
	uint32 duty_percent = 0;
	clock += 1;
	readMashTempFailed = readDS18B20(onewire_handle, &mashTemp);
	readRimsTempFailed = readDS18B20(onewire_handle_rims, &rimsTemp);
	printf("Mash temp: %d\r\n", (int)mashTemp);
	printf("RIMS temp: %d\r\n", (int)rimsTemp);
	switch(mash_state)
	{
		case MASH_STATE_NOT_STARTED:
		break;

		case MASH_HEAT_UP:
			if(rimsTemp > 90.0)
			{
				duty_percent = 0;
			}
			else
			{
				duty_percent = 100;
			}
			if(mashTemp >= mash_schedule[0].temp)
			{
				mash_state = MASH_IN;
			}
			break;

		case MASH_IN:
			break;

		case MASH_STATE_ONGOING:
			timeCollector = mash_schedule[0].duration*60;
			while(clock > timeCollector)
			{
				idx++;
				if(idx < MAX_MASH_ENTRIES)
					timeCollector += mash_schedule[idx].duration * 60;
				else
				{
					idx = -1;
					break;
				}
			}
			if(-1 == idx)
			{
				printf("Mash has completed\n");
				mash_state = MASH_STATE_COMPLETED;
				nrFailedReadTemps = 0;
			}
			else
			{
				if(readMashTempFailed || readRimsTempFailed)
				{
					nrFailedReadTemps++;
					if(nrFailedReadTemps > 3)
					{
						nrFailedReadTemps = 0;
						duty_percent = 0.0;
					}
				}
				else
				{
					setMashSetTemp(mash_schedule[idx].temp);
					duty_percent = calculateDuty(getMashSetTemp(), mashTemp, rimsTemp);
				}
			}
			break;

		case MASH_STATE_COMPLETED:
			break;

		case SPARGE_ONGOING:
			if(readMashTempFailed || readRimsTempFailed)
			{
				nrFailedReadTemps++;
				if(nrFailedReadTemps > 3)
				{
					nrFailedReadTemps = 0;
					duty_percent = 0.0;
				}
			}
			else
			{
				setMashSetTemp(78);
				duty_percent = calculateDuty(getMashSetTemp(), mashTemp, rimsTemp);
			}
			break;
	}
	//printf("Clock: %u , Duty: %u , Period: %u Mashtemp %f\n", clock,  pwm_get_duty(0), pwm_get_period(), mashTemp);
	//duty_percent = 50;
	duty_dig = duty_percent * 1023 / 100;
	printf("Duty %u, Duty_perc %u, Mash state %d\n", duty_dig, duty_percent, mash_state);
	pwm_set_duty(duty_dig,0);
	pwm_start();
	startConversionResult = ds18b20_startConversion(onewire_handle);
	startConversionResult = ds18b20_startConversion(onewire_handle_rims);
	//printf("startConversionResult %d\n",startConversionResult);
}

void start_mash(int skipMashIn)
{
	clock = 0;
	setMashSetTemp(mash_schedule[0].temp);
	if(skipMashIn)
		mash_state = MASH_STATE_ONGOING;
	else
		mash_state = MASH_HEAT_UP;
}

void abort_mash(void)
{
	clock = 0;
	mash_state = MASH_STATE_NOT_STARTED;
}

void mash_in_completed(void)
{
	mash_state = MASH_STATE_ONGOING;
	clock = 0;
}

void mash_completed(void)
{
	mash_state = SPARGE_ONGOING;
}

mash_states getMashState(void)
{
	return mash_state;
}

void setMashEntry(unsigned int idx, unsigned int temp, unsigned int duration)
{
	if(idx < MAX_MASH_ENTRIES)
	{
		mash_schedule[idx].temp = (float)temp;
		mash_schedule[idx].duration = duration;
	}
}

mash_entry_type getMashEntry(unsigned int idx)
{
	if(idx < MAX_MASH_ENTRIES)
	{
		return mash_schedule[idx];
	}
	return nullmash;
}

unsigned int getMashClock(void)
{
	return clock;
}

void mash_regulator_init(void)
{
	uint32 duty[] = {0,0};
	uint32 io_info[][3] = {{PERIPHS_IO_MUX_GPIO4_U, FUNC_GPIO4, 0}, {GPIO_PIN_REG_15, FUNC_GPIO15, 1}};
//	uint32 io_info[][3] = {{PERIPHS_IO_MUX_SD_DATA1_U, FUNC_GPIO8, 0}};
	int startConversionResult;

	GPIO_AS_OUTPUT(GPIO_Pin_4);
	GPIO_AS_OUTPUT(GPIO_Pin_15);
	pwm_init(1000000, duty, 2, io_info);

	onewire_handle = onewire_init(5);
	onewire_handle_rims = onewire_init(12);
	printf("onewire_handle %d\n",onewire_handle);
	startConversionResult = ds18b20_startConversion(onewire_handle);
	printf("startConversionResult %d\n",startConversionResult);
	startConversionResult = ds18b20_startConversion(onewire_handle_rims);
	printf("startConversionResult %d\n",startConversionResult);
	mash_timer = xTimerCreate
            ( /* Just a text name, not used by the RTOS
              kernel. */
              "MashTimer",
              /* The timer period in ticks, must be
              greater than 0. */
              100,
              /* The timers will auto-reload themselves
              when they expire. */
              pdTRUE,
              /* The ID is used to store a count of the
              number of times the timer has expired, which
              is initialised to 0. */
              ( void * ) 0,
              /* Each timer calls the same callback when
              it expires. */
			  vMashTimerCallback
            );
	if(NULL != mash_timer)
	{
		if( xTimerStart( mash_timer, 0 ) != pdPASS )
		{
			printf("mash_regulator_init failed to start timer\n");
		}
		else
		{
			printf("mash_regulator_init successful\n");
		}
	}
	else
	{
		printf("mash_regulator_init failed\n");
	}
}

