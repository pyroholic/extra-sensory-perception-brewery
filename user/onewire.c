#include "onewire.h"
#include "c_types.h"
#include "espressif/esp8266/eagle_soc.h"
#include "gpio.h"
#include "espressif/esp8266/pin_mux_register.h"
#include <stdint.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

/* Private type definitions */
typedef enum {
   uninitialized,
   initialized,
} OneWire_state_t;

typedef struct {
   unsigned short  pin;
   OneWire_state_t state;
} OneWire_t;

#define FUNC_GPIO_PIN(i) \
    (i==0) ? FUNC_GPIO0:  \
    (i==1) ? FUNC_GPIO1:  \
    (i==2) ? FUNC_GPIO2:  \
    (i==3) ? FUNC_GPIO3:  \
    (i==4) ? FUNC_GPIO4:  \
    (i==5) ? FUNC_GPIO5:  \
    (i==6) ? FUNC_GPIO6:  \
    (i==7) ? FUNC_GPIO7:  \
    (i==8) ? FUNC_GPIO8:  \
    (i==9) ? FUNC_GPIO9:  \
    (i==10)? FUNC_GPIO10: \
    (i==11)? FUNC_GPIO11: \
    (i==12)? FUNC_GPIO12: \
    (i==13)? FUNC_GPIO13: \
    (i==14)? FUNC_GPIO14: \
    		FUNC_GPIO15


#define DS1820_WRITE_SCRATCHPAD 	0x4E
#define DS1820_READ_SCRATCHPAD      0xBE
#define DS1820_COPY_SCRATCHPAD 		0x48
#define DS1820_READ_EEPROM 			0xB8
#define DS1820_READ_PWRSUPPLY 		0xB4
#define DS1820_SEARCHROM 			0xF0
#define DS1820_SKIP_ROM             0xCC
#define DS1820_READROM 				0x33
#define DS1820_MATCHROM 			0x55
#define DS1820_ALARMSEARCH 			0xEC
#define DS1820_CONVERT_T            0x44

#define OUT_LOW  0
#define OUT_HIGH  1

/* Declarations of local data */
static int onewire_slots[4] = {-1, -1, -1, -1};
static OneWire_t onewire[4];

static const uint8_t crc_table[] = {
      0, 94,188,226, 97, 63,221,131,194,156,126, 32,163,253, 31, 65,
      157,195, 33,127,252,162, 64, 30, 95,  1,227,189, 62, 96,130,220,
      35,125,159,193, 66, 28,254,160,225,191, 93,  3,128,222, 60, 98,
      190,224,  2, 92,223,129, 99, 61,124, 34,192,158, 29, 67,161,255,
      70, 24,250,164, 39,121,155,197,132,218, 56,102,229,187, 89,  7,
      219,133,103, 57,186,228,  6, 88, 25, 71,165,251,120, 38,196,154,
      101, 59,217,135,  4, 90,184,230,167,249, 27, 69,198,152,122, 36,
      248,166, 68, 26,153,199, 37,123, 58,100,134,216, 91,  5,231,185,
      140,210, 48,110,237,179, 81, 15, 78, 16,242,172, 47,113,147,205,
      17, 79,173,243,112, 46,204,146,211,141,111, 49,178,236, 14, 80,
      175,241, 19, 77,206,144,114, 44,109, 51,209,143, 12, 82,176,238,
      50,108,142,208, 83, 13,239,177,240,174, 76, 18,145,207, 45,115,
      202,148,118, 40,171,245, 23, 73,  8, 86,180,234,105, 55,213,139,
      87,  9,235,181, 54,104,138,212,149,203, 41,119,244,170, 72, 22,
      233,183, 85, 11,136,214, 52,106, 43,117,151,201, 74, 20,246,168,
      116, 42,200,150, 21, 75,169,247,182,232, 10, 84,215,137,107, 53};

/* Definitions */


/* Private functions */

/**
 * @brief Write one bit
 *        Writes one bit to the OneWire bus
 * @param ow: OneWire instance50
 *
 * This function writes one bit to the OneWire bus. The micro controller pulls
 * the GPIO pin assigned to the OneWire instance 'low' to start the transaction,
 * followed by either releasing the pin or keeping it pulled low to write the data.
 * The GPIO pin needs to be pulled low 1-15us before releasing to write a '1'.
 */

static void onewire_writeBit(int ow, uint8_t bit) {
  taskDISABLE_INTERRUPTS();
          os_delay_us(1);
	  //Let bus get pulled high
	  GPIO_DIS_OUTPUT(onewire[ow].pin);


	  if (bit) {
	      //5us low pulse
	      GPIO_OUTPUT_SET(onewire[ow].pin,OUT_LOW);
	      os_delay_us(10);
	      //let bus go high again (device should read about 30us later)
	      GPIO_DIS_OUTPUT(onewire[ow].pin);		  // was OW_RLS	// input
	      os_delay_us(55);
	  }else{
	      //55us low
	      GPIO_OUTPUT_SET(onewire[ow].pin,OUT_LOW);
	      os_delay_us(65);
	      //5us high
	      GPIO_DIS_OUTPUT(onewire[ow].pin);		  // was OW_RLS	// input
	      os_delay_us(5);
	  }
	  taskENABLE_INTERRUPTS();

}

/**
 *  @brief Read one bit
 *         Reads one bit from the OneWire bus
 *  @param ow: OneWire instance
 *
 *  This function reads one bit from the OneWire bus
 *  The micro controller pulls the GPIO pin assigned to the OneWire instance,
 *  releases the pin and reads the data. The OneWire bus needs to be pulled
 *  down minimum 1us. The read data is valid for 15us from the start of the
 *  transaction. Total transaction time is 60-120us.
 */

static uint8_t onewire_readBit(int ow) {
	  uint8_t bit=0;
	  taskDISABLE_INTERRUPTS();
	  os_delay_us(1);
	  //Pull bus low for 15us
	  GPIO_OUTPUT_SET(onewire[ow].pin,OUT_LOW);
	  os_delay_us(3);

	  //Allow device to control bu
	  GPIO_DIS_OUTPUT(onewire[ow].pin);
	  os_delay_us(10);

	  //Read value
	  if (GPIO_INPUT_GET(onewire[ow].pin)){		 //was if (OW_IN)
	    bit = 1;
	  }
	  //Wait for end of bit
	  os_delay_us(53);
	  taskENABLE_INTERRUPTS();
	  return bit;
}

void onewire_writeByte(int ow, uint8_t byte) {
   int i;
   for(i=0;i<8;i++) {
      onewire_writeBit(ow, byte&0x1);
      byte >>= 1;
   }
}

uint8_t onewire_readByte(int ow) {
   int i;
   uint8_t byte = 0;
   for(i=0;i<8;i++) {
      byte >>= 1;
      byte |= (onewire_readBit(ow) << 7);
   }
   return byte;
}

/* Public functions */

/**
 * @breif Initializes a OneWire instance
 *
 * @param GPIOx: GPIO port where OneWire devices are connected
 * @param pin: GPIO pin where OneWire devices are connected
 *
 * Initializes a OneWire instance, assigns the GPIO port and Pin
 * to the instance and returns instance number for use in application
 * code.
 */

int onewire_init(unsigned short pin) {
   int i;
   int ret = -1;
   /* Find empty onewire slot */
   for(i=0;i<4;i++) {
      if (onewire_slots[i] == -1) {
         /* Reserve slot */
    	  printf("Onewire slot %d\n",i);
         onewire_slots[i] = 1;
         /* Initialize GPIO pin */
//		 PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO5_U, FUNC_GPIO5);
//       PIN_PULLUP_EN(PERIPHS_IO_MUX_GPIO5_U);
		 PIN_FUNC_SELECT(GPIO_PIN_REG(pin), FUNC_GPIO_PIN(pin));
         PIN_PULLUP_EN(GPIO_PIN_REG(pin));
         /* Update OneWire slot */
         onewire[i].pin = pin;
         onewire[i].state = uninitialized;
         ret = i;
         break;
      }
   }
   return ret;
}

/**
 * @breif Resets OneWire instance.
 * @param ow: OneWire instance
 *
 * Resets a OneWire instance by sending a reset pulse.
 * @retval Returns 0 if a presence pulse is detected, otherwise -1 to indicate fault.
 */

int onewire_reset(int ow) {
    //Hold the bus low for 500us
    GPIO_OUTPUT_SET(onewire[ow].pin,OUT_LOW); //was OW_LO
    os_delay_us(500);

    //Give up control of the bus and wait for a device to reply
    GPIO_DIS_OUTPUT(onewire[ow].pin); //set to input
    os_delay_us(80);

    //The bus should be low if the device is present:
    if (GPIO_INPUT_GET(onewire[ow].pin)){
        //No device
	return 1;
    }

    //The device should have stopped pulling the bus now:
    os_delay_us(300);
    if (! GPIO_INPUT_GET(onewire[ow].pin)) {
        //Something's wrong, the bus should be high
        return 2;
    }
    onewire[ow].state = initialized;
    //All good.
    return 0;
}

uint8_t onewire_crc8(uint8_t * data, int size) {
   uint8_t crc = 0;
   while (size--) {
      crc = crc_table[crc ^ *data];
      data++;
   }
   return crc;
}


int readDS18B20(int ow, float *temperature)
{
	uint8_t scratchpad[9];
	uint8_t crc;
	int test;
	int k;
	int retVal = -1;

	test = onewire_reset(ow);
	printf("test %d\n");
    onewire_writeByte(ow, DS1820_SKIP_ROM);  // skip ROM command
    onewire_writeByte(ow, DS1820_CONVERT_T); // convert T command

    os_delay_us(750);
	onewire_reset(ow);
    onewire_writeByte(ow, DS1820_SKIP_ROM);		// skip ROM command
    onewire_writeByte(ow, DS1820_READ_SCRATCHPAD); // read scratchpad command

    for (k=0;k<9;k++){
    	scratchpad[k]=onewire_readByte(ow);
    }
    //os_printf("\n ScratchPAD DATA = %X %X %X %X %X %X %X %X %X\n",get[8],get[7],get[6],get[5],get[4],get[3],get[2],get[1],get[0]);

    crc = onewire_crc8(scratchpad, 8);
    if(crc != scratchpad[8])
    {
    	printf("CRC check failed: %02X %02X\n", scratchpad[8], crc);
    	*temperature = -273.15;
    }
    else
    {
    	*temperature = (float)(scratchpad[1] << 8 | scratchpad[0])/16.0;
    	retVal = 0;
    }

    return retVal;
}

int ds18b20_startConversion(int ow) {
   if (onewire_reset(ow)) {
      return -1;
   }
   /* Send reset pulse */
   onewire_writeByte(ow, DS1820_SKIP_ROM);
   /* Send conversion command */
   onewire_writeByte(ow, 0x44);
   return 0;
}
