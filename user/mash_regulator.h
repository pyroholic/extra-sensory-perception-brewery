/*
 * mash_regulator.h
 *
 *  Created on: Apr 16, 2017
 *      Author: julle
 */

#ifndef USER_MASH_REGULATOR_H_
#define USER_MASH_REGULATOR_H_

#define MAX_MASH_ENTRIES 5

typedef struct mash_entry_type
{
	float temp;
	unsigned int duration;
} mash_entry_type;

typedef enum
{
	MASH_STATE_NOT_STARTED,
	MASH_HEAT_UP,
	MASH_IN,
	MASH_STATE_ONGOING,
	MASH_STATE_COMPLETED,
	SPARGE_ONGOING
} mash_states ;


mash_states getMashState(void);

void start_mash(int skipMashIn);

void abort_mash(void);

void mash_in_completed(void);

void mash_completed(void);

mash_entry_type getMashEntry(unsigned int idx);

unsigned int getMashClock(void);

float getMashSetTemp(void);

float getMashActualTemp(void);

float getRimsActualTemp(void);

#endif /* USER_MASH_REGULATOR_H_ */
