#include <nopoll/nopoll.h>
#include "espconn.h"
#include "esp_wifi.h"
#include "ssl_compat-1.0.h"
#include "esp_common.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include "mash_regulator.h"
#include "pwm.h"

typedef struct reqqueue_entry_t
{
	char *pusrData;
	struct espconn * conn;
} reqqueue_entry_t;

#define PAGEBUFFER_SIZE 1500
char pagebuffer[PAGEBUFFER_SIZE];
xQueueHandle reqQueue;
unsigned int boilPwm = 0;


#define HTTP_200 "HTTP/1.1 200 OK\r\n"

enum
{
	START_MASH = 1,
	SAVE_MASH_SCHEDULE = 2,
	ABORT_MASH = 3,
	MASH_IN_COMPLETED = 4,
	MASH_COMPLETED = 5
};

void webserver_listen(void *arg);
void webserver_recv(void *arg, char *pusrdata, unsigned short length);
void webserver_recon(void *arg, sint8 err);
void webserver_discon(void *arg);
void webserver_sent(void *arg);
void replyWithDefaultContent(void)
{
	int str_pointer = 0;
	memset(pagebuffer,0,PAGEBUFFER_SIZE);
	str_pointer += sprintf(pagebuffer,"%s\r\n",HTTP_200); 
	str_pointer += sprintf(&pagebuffer[str_pointer],"%s","<html><body><a href=\"/mash\">Mash</a><br><a href=\"/boil\">Boil</a></body></html>");
}

int parseUIntParam(const char* src, const char *paramName, unsigned int *parsedVal)
{
	char buf[100];
	char *p = strstr(src, paramName);
	int retVal = 0;

	if(NULL != p)
	{
		sprintf(buf, "%s%%u", paramName);
		if(1 == sscanf(p, buf, parsedVal))
		{
			retVal = 1;
		}
	}
	return retVal;
}

void parseMashData(char *pusrdata)
{
	char *p = strstr(pusrdata,"action=");
	//char temp_string[10];
	if(p != NULL)
	{
		char buf[100];
		unsigned int ut, ud, action;
		unsigned int ui;
		unsigned int skipMashIn = 0;

		if(MASH_STATE_NOT_STARTED == getMashState())
		{
			for(ui=0; ui<MAX_MASH_ENTRIES; ui++)
			{
				sprintf(buf, "temp%u=", ui);
				if(parseUIntParam(pusrdata, buf, &ut))
				{
					sprintf(buf, "dur%u=", ui);
					if(parseUIntParam(pusrdata, buf, &ud))
					{
						printf("Temp%u=%u, Duration%u=%u\n", ui, ut, ui, ud);
						setMashEntry(ui, ut, ud);
					}
				}
			}
		}

		sprintf(buf, "skipMashIn=", ui);
		(void)parseUIntParam(pusrdata, buf, &skipMashIn);

		sprintf(buf, "action=", ui);
		if(parseUIntParam(pusrdata, buf, &action))
		{
			switch(action)
			{
			case START_MASH:
				start_mash(skipMashIn);
				break;
			case SAVE_MASH_SCHEDULE:
				break;
			case ABORT_MASH:
				abort_mash();
				break;
			case MASH_IN_COMPLETED:
				mash_in_completed();
				break;
			case MASH_COMPLETED:
				mash_completed();
				break;
			}
		}
	}
}

void parseBoilData(char *pusrdata)
{
	char *p = strstr(pusrdata,"action=");
  unsigned int duty_dig;
  
	if(p != NULL)
	{
		char buf[100];

		sprintf(buf, "boilPwm=");
		if(parseUIntParam(pusrdata, buf, &boilPwm))
		{
		  	duty_dig = boilPwm * 1023 / 100;
		  	pwm_set_duty(duty_dig,1);
	      pwm_start();
		}
	}
}

int printClock(char * str)
{
	unsigned int clock = getMashClock();
	unsigned int secInHour = clock%3600;
	sprintf(str, "%02u:%02u:%02u", clock/3600, secInHour/60, secInHour%60);
	printf(str);
	return strlen(str);
}

void replyWithBoilContent(char *pusrdata)
{
	int str_pointer = 0;
	parseBoilData(pusrdata);
  memset(pagebuffer,0,PAGEBUFFER_SIZE);
 	str_pointer += sprintf(pagebuffer,"%s\r\n",HTTP_200); 
  str_pointer += sprintf(&pagebuffer[str_pointer],"<html><body>");
	str_pointer += sprintf(&pagebuffer[str_pointer],"<form action=\"/boil\" method=\"get\">");
	str_pointer += sprintf(&pagebuffer[str_pointer],"  <table><tr><th>Boil pwm %</th></tr>");
	str_pointer += sprintf(&pagebuffer[str_pointer],"  <tr><td><input type=\"text\" name=\"boilPwm\" value=\"%d\"></td></tr>", boilPwm);
	str_pointer += sprintf(&pagebuffer[str_pointer],"  </table><br><br>");
	str_pointer += sprintf(&pagebuffer[str_pointer],"  <button name=\"action\" action type=\"submit\" value=\"8\">Save</button>");
	str_pointer += sprintf(&pagebuffer[str_pointer],"</form> ");
	str_pointer += sprintf(&pagebuffer[str_pointer],"</body></html>");

}

void replyWithMashContent(char *pusrdata)
{
	int str_pointer = 0;
	int i = 0;

	parseMashData(pusrdata);
	memset(pagebuffer,0,PAGEBUFFER_SIZE);
	str_pointer += sprintf(pagebuffer,"%s\r\n",HTTP_200); 
	if(MASH_STATE_NOT_STARTED == getMashState())
	{
		str_pointer += sprintf(&pagebuffer[str_pointer],"<html><body>");
		str_pointer += sprintf(&pagebuffer[str_pointer],"<form action=\"/mash\" method=\"get\">");
		str_pointer += sprintf(&pagebuffer[str_pointer],"<input type=\"checkbox\" name=\"skipMashIn\" value=\"1\"> Skip mash in<br>");
		str_pointer += sprintf(&pagebuffer[str_pointer],"  <table><tr><th>Mash temp</th><th>Duration (min)</th></tr>");
		for(i=0; i<MAX_MASH_ENTRIES;i++)
		{
			str_pointer += sprintf(&pagebuffer[str_pointer],"  <tr><td><input type=\"text\" name=\"temp%d\" value=\"%.0f\"></td><td><input type=\"text\" name=\"dur%d\" value=\"%u\"></td></tr>", i, getMashEntry(i).temp, i, getMashEntry(i).duration);
		}
		str_pointer += sprintf(&pagebuffer[str_pointer],"  </table><br><br>");
		str_pointer += sprintf(&pagebuffer[str_pointer],"  <button name=\"action\" action type=\"submit\" value=\"2\">Save</button>");
		str_pointer += sprintf(&pagebuffer[str_pointer],"  <br>");
		str_pointer += sprintf(&pagebuffer[str_pointer],"  <button name=\"action\" action type=\"submit\" value=\"1\">Start mash</button>");
		str_pointer += sprintf(&pagebuffer[str_pointer],"</form> ");
		str_pointer += sprintf(&pagebuffer[str_pointer],"</body></html>");
	}
	else
	{
		str_pointer += sprintf(&pagebuffer[str_pointer],"<html><body onload=\"setTimeout(function(){window.location = '/mash';}, 1000)\">");
		str_pointer += printClock(&pagebuffer[str_pointer]);
		str_pointer += sprintf(&pagebuffer[str_pointer],"<br>");
		str_pointer += sprintf(&pagebuffer[str_pointer],"Actual temperature: %.2f<br>", getMashActualTemp());
		str_pointer += sprintf(&pagebuffer[str_pointer],"Set temperature: %.2f<br>", getMashSetTemp());
		str_pointer += sprintf(&pagebuffer[str_pointer],"Actual RIMS temperature: %.2f<br>", getRimsActualTemp());
		str_pointer += sprintf(&pagebuffer[str_pointer],"  <table><tr><th>Set mash temp</th><th>Duration (min)</th></tr>");
		for(i=0; i<MAX_MASH_ENTRIES;i++)
		{
			str_pointer += sprintf(&pagebuffer[str_pointer],"  <tr><td>%.0f</td><td>%u min</td></tr>", getMashEntry(i).temp, getMashEntry(i).duration);
		}
		str_pointer += sprintf(&pagebuffer[str_pointer],"  </table><br><br>");
		str_pointer += sprintf(&pagebuffer[str_pointer],"<form action=\"/mash\" method=\"get\">");
		if(getMashState() == MASH_HEAT_UP || getMashState() == MASH_IN)
		{
			str_pointer += sprintf(&pagebuffer[str_pointer],"  <button name=\"action\" action type=\"submit\" value=\"4\">Continue mash</button>");
		}
		if(getMashState() == MASH_STATE_COMPLETED)
		{
			str_pointer += sprintf(&pagebuffer[str_pointer],"  <button name=\"action\" action type=\"submit\" value=\"5\">Start sparging</button>");
		}
		str_pointer += sprintf(&pagebuffer[str_pointer],"  <button name=\"action\" action type=\"submit\" value=\"3\">Abort mash</button>");
		str_pointer += sprintf(&pagebuffer[str_pointer],"</form> ");
		str_pointer += sprintf(&pagebuffer[str_pointer],"</body></html>");
	}
	printf("Str_pointer %d\n",str_pointer);
}

void webserver_init(int port) 
{
    LOCAL struct espconn esp_conn;
    LOCAL esp_tcp esptcp;
    //Fill the connection structure, including "listen" port
    esp_conn.type = ESPCONN_TCP;
    esp_conn.state = ESPCONN_NONE;
    esp_conn.proto.tcp = &esptcp;
    esp_conn.proto.tcp->local_port = port;
    esp_conn.recv_callback = NULL;
    esp_conn.sent_callback = NULL;
    esp_conn.reserve = NULL;
    //Register the connection timeout(0=no timeout)
    espconn_regist_time(&esp_conn,0,0);
    //Register connection callback
    espconn_regist_connectcb(&esp_conn, webserver_listen);
    //Start Listening for connections
    espconn_accept(&esp_conn); 
    printf("Web Server initialized: Type = SDK API\n");
}

void webserver_listen(void *arg)
{
    struct espconn *pesp_conn = (struct espconn *)arg;

    printf("Max tcp %u\n",espconn_tcp_get_max_con());
    espconn_regist_recvcb(pesp_conn, webserver_recv);
    espconn_regist_reconcb(pesp_conn, webserver_recon);
    espconn_regist_disconcb(pesp_conn, webserver_discon);
    espconn_regist_sentcb(pesp_conn, webserver_sent);
}

void webserver_recv(void *arg, char *pusrdata, unsigned short length)
{
	/*
	struct espconn *ptrespconn = ( struct espconn *)arg;
	int a;
	printf("%s\n",pusrdata);
	if(strncmp(pusrdata, "GET /mash", strlen("GET /mash")) == 0)
	{
		replyWithMashContent(pusrdata);
	}
	else
	{
		replyWithDefaultContent();
	}
	a = espconn_send(ptrespconn, pagebuffer, strlen(pagebuffer));
	printf("espconn_send: %d\n",a);
	espconn_disconnect(ptrespconn);
	*/
	reqqueue_entry_t entry;
	struct espconn *ptrespconn = ( struct espconn *)arg;
	entry.conn = ptrespconn;
	entry.pusrData = pusrdata;
	xQueueSendToBackFromISR(reqQueue, &entry, NULL);
}

void webserver_recon(void *arg, sint8 err)
{
	struct espconn *pesp_conn = ( struct espconn *)arg;

	printf("webserver's %d.%d.%d.%d:%d err %d reconnect\n", pesp_conn->proto.tcp->remote_ip[0],
        pesp_conn->proto.tcp->remote_ip[1],pesp_conn->proto.tcp->remote_ip[2],
        pesp_conn->proto.tcp->remote_ip[3],pesp_conn->proto.tcp->remote_port, err);
}

void webserver_discon(void *arg)
{
    struct espconn *pesp_conn = ( struct espconn *)arg;

    printf("webserver's %d.%d.%d.%d:%d disconnect\n", pesp_conn->proto.tcp->remote_ip[0],
            pesp_conn->proto.tcp->remote_ip[1],pesp_conn->proto.tcp->remote_ip[2],
            pesp_conn->proto.tcp->remote_ip[3],pesp_conn->proto.tcp->remote_port);
}

void webserver_sent(void *arg)
{
    struct espconn *pesp_conn = ( struct espconn *)arg;

    printf("webserver's %d.%d.%d.%d:%d sent\n", pesp_conn->proto.tcp->remote_ip[0],
            pesp_conn->proto.tcp->remote_ip[1],pesp_conn->proto.tcp->remote_ip[2],
            pesp_conn->proto.tcp->remote_ip[3],pesp_conn->proto.tcp->remote_port);
    espconn_disconnect(pesp_conn);
}
LOCAL void webserver_task(void *pvParameters)
{
	portBASE_TYPE xStatus;
	struct ip_info ip_config;
	struct station_config sta_config;
	bzero(&sta_config, sizeof(struct station_config));

	sprintf(sta_config.ssid, "ComHem<BA7A48>");
	sprintf(sta_config.password, "8dcb6906");
	wifi_station_set_config(&sta_config);
	os_printf("%s\n", __func__);
	wifi_get_ip_info(STATION_IF, &ip_config);
	while(ip_config.ip.addr == 0){
		vTaskDelay(1000 / portTICK_RATE_MS);
		wifi_get_ip_info(STATION_IF, &ip_config);
	}
	printf("ip_config.ip.addr %d\n",ip_config.ip.addr);
	webserver_init(80);

	vTaskDelete(NULL);
	printf("delete the websocket_task\n");
}

LOCAL void worker_task(void *pvParameters)
{
	reqqueue_entry_t entry;
	while(1)
	{
		if(pdTRUE == xQueueReceive(reqQueue,&entry, 1000 / portTICK_RATE_MS))
		{
			int a;
			printf("%s\n",entry.pusrData);
			if(strncmp(entry.pusrData, "GET /mash", strlen("GET /mash")) == 0)
			{
				replyWithMashContent(entry.pusrData);
			}
			else if(strncmp(entry.pusrData, "GET /boil", strlen("GET /boil")) == 0)
			{
			  replyWithBoilContent(entry.pusrData);
			}
			else
			{
				replyWithDefaultContent();
			}
			a = espconn_send(entry.conn, pagebuffer, strlen(pagebuffer));
			printf("espconn_send: %d\n",a);
			//espconn_disconnect(entry.conn);
		}
		else
			printf("Timeout\n");
	}
}

void webserver_start(void *optarg)
{
	printf("Start webserver\n");
	xTaskCreate(webserver_task, "webserver_task", 512, optarg, 4, NULL);
	printf("Started webserver\n");
	reqQueue = xQueueCreate( 10, sizeof( reqqueue_entry_t ) );
	xTaskCreate(worker_task, "worker_task", 512, optarg, 4, NULL);
}

/* end-of-file-found */
